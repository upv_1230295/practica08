package com.example.fernando.practica08;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Fernando on 15/06/2015.
 */
public class SMSActivity extends Activity{

    EditText txtTelefono;
    Button btnEnviar;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_activity);

        txtTelefono = (EditText)findViewById(R.id.txtTelefono);
        btnEnviar = (Button)findViewById(R.id.btnEnviar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            data = extras.getString("DATA");
        }

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String telefono = txtTelefono.getText().toString();

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(telefono, null, data, null, null);

                Toast.makeText(getBaseContext(), "Mensaje Enviado.", Toast.LENGTH_LONG).show();
            }
        });

    }
}
